package sample.models;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class GameBoard {

    private final List<Card> cards;
    private final List<Card> selectedCards;
    private final GridPane grid;
    private boolean eventsBlocked;

    public GameBoard(List<Card> cards) {

        this.cards = cards;
        this.selectedCards = new ArrayList<>();
        this.grid = new GridPane();
        eventsBlocked = false;

        // Mélangez les cartes
        Collections.shuffle(cards);

        // Ajoutez les cartes au plateau de jeu
        int index = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                Card card = cards.get(index);
                ImageView imageView = card.getImageView();
                imageView.setOnMouseClicked(event -> handleCardClick(card));
                grid.add(imageView, j, i);
                index++;
            }
        }
    }

    public Node getNode() {
        return grid;
    }

    /*
     * Fonction permettant de retourner les cartes et les comparer entre elles
     */
    private void handleCardClick(Card card) {

        // SI la carte à déja été apparéillée ou que les evenements sont bloqués, on ignore le click
        if (card.isMatched() || eventsBlocked) {
            return;
        }

        if (!card.isRevealed()) {
            selectedCards.add(card);
        } else {
            selectedCards.remove(card);
        }

        card.flip();

        if (selectedCards.size() == 2) {

            Card first = selectedCards.get(0);
            Card second = selectedCards.get(1);

            // Si deux cartes ont été sélectionnées, vérifiez si elles sont appariées
            if (first.getFrontImage() == second.getFrontImage()) {
                first.setMatched(true);
                second.setMatched(true);
                checkGameWon();
            } else {
                eventsBlocked = true;
                // Sinon, retournez les cartes (on bloque les events durant ce temps pour éviter les bugs)
                Timeline timeline = new Timeline(
                    new KeyFrame(Duration.seconds(1), event -> {
                        first.flip();
                        second.flip();
                        eventsBlocked = false;
                    })
                );
                timeline.play();
            }
            selectedCards.clear();
        }
    }

    /*
     * Fonction permettant de vérifier si la partie est gagnée et de la réinitialiser
     */
    private void checkGameWon() {
        // Vérifiez si toutes les cartes ont été appariées
        boolean gameWon = true;
        for (Card card : cards) {
            if (!card.isMatched()) {
                gameWon = false;
                break;
            }
        }

        if (gameWon) {
            // Affichez un message de victoire et proposez au joueur de recommencer
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Félicitations, vous avez gagné ! Voulez-vous recommencer ?");
            ButtonType buttonTypeYes = new ButtonType("Oui");
            ButtonType buttonTypeNo = new ButtonType("Non");
            alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeYes) {
                // Si le joueur a choisi de recommencer, rechargez le plateau de jeu avec une nouvelle liste de cartes mélangées
                Collections.shuffle(cards);
                int index = 0;
                for (int i = 0; i < 4; i++) {
                    for (int j = 0; j < 5; j++) {
                        Card card = cards.get(index);
                        ImageView imageView = card.getImageView();
                        imageView.setOnMouseClicked(event -> handleCardClick(card));
                        grid.add(imageView, j, i);
                        index++;
                    }
                }
            }
        }
    }
}
