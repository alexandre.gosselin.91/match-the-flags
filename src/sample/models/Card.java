package sample.models;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Card {

    private final Image frontImage;
    private final Image backImage;
    private final ImageView imageView;
    private boolean isMatched;
    private boolean isRevealed;

    public Card(Image frontImage, Image backImage) {
        this.frontImage = frontImage;
        this.backImage = backImage;
        this.imageView = new ImageView(backImage);
        imageView.setFitWidth(100);
        imageView.setFitHeight(100);
        this.isMatched = false;
        this.isRevealed = false;
    }

    public Image getFrontImage() { return frontImage; }

    public Image getBackImage() { return backImage; }

    public ImageView getImageView() { return imageView; }

    public boolean isMatched() { return isMatched; }

    public void setMatched(boolean matched) { isMatched = matched; }

    public boolean isRevealed() { return isRevealed; }

    public void setRevealed(boolean revealed) { isRevealed = revealed; }

    public void flip() {
        if (imageView.getImage() == backImage) {
            imageView.setImage(frontImage);
            setRevealed(true);
        } else {
            imageView.setImage(backImage);
            setRevealed(false);
        }
    }
}

