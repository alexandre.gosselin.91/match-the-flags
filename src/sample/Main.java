package sample;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import sample.models.Card;
import sample.models.GameBoard;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    public void start(Stage primaryStage) {

        List<Card> cards = new ArrayList<>();

        // On va chercher les images
        String folderPath = "assets";
        File folder = new File(folderPath);
        File[] files = folder.listFiles();

        // Pour chaque fichier, créez deux cartes
        for (File file : files) {
            // Vérifiez que le fichier est une image
            if (file.isFile() && file.getName().endsWith(".png") && !file.getName().startsWith("back")) {
                Image frontImage = new Image(file.toURI().toString());
                Image backImage = new Image("file:backCardImage.png");
                cards.add(new Card(frontImage, backImage));
                cards.add(new Card(frontImage, backImage));
            }
        }

        // Créez le plateau de jeu
        GameBoard gameBoard = new GameBoard(cards);

        // Créez une scène avec le plateau de jeu
        Scene scene = new Scene((Parent) gameBoard.getNode(), 500, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
